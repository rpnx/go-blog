package blogbackend

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/alexedwards/argon2id"
	"github.com/google/uuid"
	"log"
	"net/http"
	"time"
)

type BlogServerBackend struct {
	db                      *sql.DB
	stmtGetUserIDByUsername *sql.Stmt
	stmtGetUserData         *sql.Stmt
}

func (s *BlogServerBackend) Close() {
	if s.db != nil {
		s.db.Close()
		s.db = nil
	}
}

func (s *BlogServerBackend) GetUserIDByUsername(username string) (UserID, error) {
	rows, err := s.stmtGetUserIDByUsername.Query(sql.Named("username", username))
	defer rows.Close()

	if err != nil {
		return 0, err
	}

	if !rows.Next() {
		err = rows.Err()
		if err != nil {
			return 0, err
		}
		return 0, errors.New("User not found")
	}

	var id UserID
	err = rows.Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (s *BlogServerBackend) getUserPWHashByID(id UserID) (string, error) {
	rows, err := s.db.Query("SELECT users.password_hash FROM users WHERE users.id = ?", id)
	defer rows.Close()
	if err != nil {
		return "", err
	}

	if !rows.Next() {
		err = rows.Err()
		if err != nil {
			return "", err
		}
		return "", errors.New("User not found")
	}

	var passwordHash string
	err = rows.Scan(&passwordHash)
	if err != nil {
		return "", err
	}

	return passwordHash, nil

}

func New(db *sql.DB) (*BlogServerBackend, error) {
	s := &BlogServerBackend{}
	var ok bool
	defer func() {
		if !ok {
			s.Close()
		}
	}()
	// Run ping to test if the database was opened.
	err := db.Ping()
	if err != nil {
		log.Printf("Failed to open database: %s", err.Error())

		return nil, err
	}

	log.Printf("Opened database successfully")
	_, err = db.Exec("CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, username TEXT UNIQUE, password_hash TEXT, phone TEXT, email TEXT, created INTEGER, lastlogin INTEGER, lastip TEXT, terminated INTEGER DEFAULT 0)")

	if err != nil {
		log.Printf("Failed to create table users: %s", err.Error())
		return nil, fmt.Errorf("failed to create table users: %w", err)
	}

	_, err = db.Exec("CREATE TABLE IF NOT EXISTS sessions (sessionid TEXT PRIMARY KEY, userid INTEGER, sessionexpires INTEGER, authlevel INTEGER DEFAULT 0, authexpires INTEGER)")

	_, err = db.Exec("CREATE TABLE IF NOT EXISTS admins (userid INTEGER PRIMARY KEY, authlevel INTEGER DEFAULT 1)")
	if err != nil {
		log.Printf("Failed to create table admins")
		return nil, fmt.Errorf("failed to create table admins: %w", err)
	}

	log.Printf("Created table users")
	_, err = db.Exec("CREATE TABLE IF NOT EXISTS log_logins (id INTEGER PRIMARY KEY, userid INTEGER, ip TEXT, time INTEGER, success INTEGER, useragent TEXT, cookie TEXT)")

	if err != nil {
		log.Printf("Failed to create table log_logins")
		return nil, fmt.Errorf("failed to create table log_logins: %w", err)
	}
	log.Printf("Created table log_logins")
	s.db = db

	s.stmtGetUserData, err = db.Prepare("SELECT users.id, users.username, password_hash, phone, email, created, lastlogin, lastip FROM users WHERE users.username = (:username)")
	if err != nil {
		return nil, err
	}
	if s.stmtGetUserData == nil {
		return nil, errors.New("Failed to prepare statement")
	}

	db.Exec("CREATE TABLE IF NOT EXISTS posts (id INTEGER PRIMARY KEY, title TEXT, content TEXT, created INTEGER, lastedit INTEGER, author INTEGER, disabled INTEGER DEFAULT 0)")

	s.stmtGetUserIDByUsername, err = db.Prepare("SELECT users.id FROM users WHERE users.username = (:username)")
	if err != nil {
		return nil, err
	}
	if s.stmtGetUserIDByUsername == nil {
		return nil, errors.New("Failed to prepare statement")
	}

	ok = true
	return s, nil

}

func (s *BlogServerBackend) PublishNewPost(author UserID, title string, content string) (PostID, error) {
	result, err := s.db.Exec("INSERT INTO posts (title, content, created, lastedit, author) VALUES (?, ?, ?, ?, ?)", title, content, time.Now().Unix(), time.Now().Unix(), author)
	if err != nil {
		return -1, err
	}
	id, err := result.LastInsertId()
	if err != nil {
		log.Printf("Failed to get last insert id: %s", err.Error())
		id = -1
	}
	return PostID(id), nil
}

func (s *BlogServerBackend) SessionPublishPost(session SessionID, title string, content string) (PostID, error) {
	author, _, err := s.GetSession(session)
	if err != nil {
		return -1, err
	}
	if author == -1 {
		return -1, errors.New("Tried to post but not logged in")
	}
	if !s.UserPermissionPost(author) {
		return -1, errors.New("User does not have permission to post")
	}

	return s.PublishNewPost(author, title, content)
}

func (s *BlogServerBackend) setPass(username string, password string) error {
	hash, err := argon2id.CreateHash(password, argon2id.DefaultParams)
	_, err = s.db.Exec("UPDATE users SET password_hash = ? WHERE username = ?", hash, username)
	return err
}

type BadPasswordError struct {
}

type BadUsernameError struct {
	message string
}

type BadEmailError struct {
	message string
}

type UserAlreadyRegisteredError struct {
}

func (u UserAlreadyRegisteredError) Error() string {
	return "User already registered"
}

func (e BadPasswordError) Error() string {
	return "Invalid password"
}

func (e BadPasswordError) HttpError() string {
	return "Invalid password"
}

func (s BadPasswordError) HttpCode() int {
	return http.StatusUnauthorized
}

type PageID uint64
type PostID int64
type UserID int64
type TimestampInt int64
type SessionID string

func (t TimestampInt) Unix() int64 {
	return int64(t)
}

func (t TimestampInt) Time() time.Time {
	return time.Unix(int64(t), 0)
}

func (t TimestampInt) String() string {
	return t.Time().String()
}

type Post struct {
	Metadata PostMetadata
	Content  string
}

type PostMetadata struct {
	ID       PageID
	Title    string
	Created  TimestampInt
	AuthorID UserID
}

type LoginOptions struct {
	// If true, the session will be remembered for a long time
	Remember bool
}

func DefaultLoginOptions() LoginOptions {
	return LoginOptions{
		Remember: false,
	}
}

// The login function takes a username and password and returns a session token if the login was successful
func (s *BlogServerBackend) Login(username string, password string, options LoginOptions) (session SessionID, err error) {
	var hash string
	var id UserID
	id, err = s.GetUserIDByUsername(username)
	if err != nil {
		return "", err
	}

	hash, err = s.getUserPWHashByID(id)
	if err != nil {
		return "", err
	}

	match, err := argon2id.ComparePasswordAndHash(password, hash)
	if err != nil {
		return "", err
	}
	if !match {
		return "", BadPasswordError{}
	}

	var expireAt *time.Time

	if !options.Remember {
		var expireAtTime time.Time = time.Now().Add(time.Hour * 24 * 2)
		expireAt = &expireAtTime
	}
	// generate session
	session, err = s.NewSession(id, expireAt)
	if err != nil {
		log.Printf("Failed to generate session: %s", err.Error())
		return
	}

	return

}

func (s *BlogServerBackend) isBanned(ip string) bool {
	return false
}

// Must be a number
// Deprecated:
func (s *BlogServerBackend) IsValidPostId(id string) bool {
	for _, c := range id {
		if c < '0' || c > '9' {
			return false
		}
	}
	return true
}

func (s *BlogServerBackend) GetPostCount() (result PostID, err error) {
	rows, err := s.db.Query("SELECT COUNT(*) FROM posts WHERE disabled = 0")
	defer rows.Close()
	if err != nil {
		return 0, err
	}
	if !rows.Next() {
		return 0, nil
	}
	err = rows.Scan(&result)
	if err != nil {
		return 0, err
	}
	return result, nil
}

func (s *BlogServerBackend) getPostContent(id PostID) (result string, err error) {
	// get the post content
	rows, err := s.db.Query("SELECT content FROM posts WHERE id = ?", id)
	defer rows.Close()
	if err != nil {
		return "", err
	}
	if !rows.Next() {
		return "", errors.New("No such post")
	}
	err = rows.Scan(&result)
	if err != nil {
		return "", err
	}
	return result, nil
}

func (s *BlogServerBackend) GetPostFullData(id PostID) (post *Post, err error) {
	post = &Post{}

	// get the post metadata
	post.Metadata, err = s.getPostMetadata(id)
	if err != nil {
		return nil, err
	}

	// get the post content
	post.Content, err = s.getPostContent(id)
	if err != nil {
		return nil, err
	}

	return post, nil
}

func (s *BlogServerBackend) shouldThrottleAddress(ip string) time.Duration {
	return 0
}

func (s *BlogServerBackend) Register(username, password, email, phone string) error {

	// check if username is valid
	if !s.isUsernameValid(username) {
		return errors.New("Invalid username")
		// Todo: More specific weberror
	}

	// check if password is valid
	if !s.isPasswordValid(password) {
		// Todo: More specific weberror
		return errors.New("Invalid password")
	}

	hashedPassword, err := argon2id.CreateHash(password, argon2id.DefaultParams)

	result, err := s.db.Exec("INSERT OR IGNORE INTO users(username, password_hash, email, created, phone) VALUES (?, ?, ?, ?, ?)", username, hashedPassword, email, time.Now().Unix(), phone)

	if err != nil {
		return err
	}

	rows, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rows != 1 {
		return UserAlreadyRegisteredError{}
	}

	return nil
}

func (s *BlogServerBackend) GetUsernameFromID(id UserID) (string, error) {
	rows, err := s.db.Query("SELECT username FROM users WHERE id = ?", id)
	defer rows.Close()
	if err != nil {
		return "", err
	}
	if !rows.Next() {
		return "", errors.New("No such user")
	}
	var username string
	err = rows.Scan(&username)
	if err != nil {
		return "", err
	}
	return username, nil
}

func (s *BlogServerBackend) recordLoginFailure2(ip string, username string, userAgent string, cookie string) (err error) {
	// Record into log_logins
	_, err = s.db.Exec("INSERT INTO log_logins (userid, ip, time, success, useragent, cookie) VALUES (?, ?, ?, ?, ?, ?)", username, ip, time.Now().Unix(), 0, userAgent, cookie)
	if err != nil {
		log.Printf("Failed to record login failure: %s", err)
	}
	return
}

func (s2 *BlogServerBackend) RecordServerError(s string, err error) {
	log.Printf("Error in %s: %s", s, err.Error())
}

func (s *BlogServerBackend) NewSession(id UserID, expire *time.Time) (result SessionID, err error) {

	// create random uuid for session
	session_cookie := uuid.New().String()
	if expire == nil {
		_, err = s.db.Exec("INSERT INTO sessions (sessionid, userid) VALUES (?, ?)", session_cookie, id)
	} else {
		_, err = s.db.Exec("INSERT INTO sessions (sessionid, userid, sessionexpires) VALUES (?, ?, ?)", session_cookie, id, expire.Unix())
	}

	if err != nil {
		return "", err
	}

	return SessionID(session_cookie), nil
}

func (s *BlogServerBackend) ExtendSession(id SessionID, newTime time.Time) error {
	_, err := s.db.Exec("UPDATE sessions SET sessionexpires = ? WHERE sessionid = ? AND sessionexpires IS NOT NULL AND sessionexpires < ?", newTime.Unix(), id, newTime.Unix())
	return err
}

func (s *BlogServerBackend) GetSession(id SessionID) (user UserID, authlevel int, err error) {
	rows, err := s.db.Query("SELECT userid, sessionexpires, authlevel, authexpires FROM sessions WHERE sessionid = ?", id)
	defer rows.Close()
	if err != nil {
		return -1, -1, err
	}
	if !rows.Next() {
		return -1, -1, errors.New("Session not found")
	}
	// dropauth is optional

	var authExpires sql.NullInt64
	var sessionExpires sql.NullInt64
	err = rows.Scan(&user, &sessionExpires, &authlevel, &authExpires)
	if err != nil {
		return -1, -1, err
	}
	if authExpires.Valid && time.Now().Unix() > authExpires.Int64 {
		authlevel = 1
	}
	if sessionExpires.Valid && time.Now().Unix() > sessionExpires.Int64 {
		return -1, -1, errors.New("Session expired")
	}
	return user, authlevel, nil

}

func (s *BlogServerBackend) isUsernameValid(username string) bool {
	if len(username) < 3 || len(username) > 40 {
		return false
	}

	// only allow asii characters
	for _, c := range username {
		if c > 127 {
			return false
		}
	}

	// only allow lowercase ascii characters and numbers
	for _, c := range username {
		if (c < 'a' || c > 'z') && (c < '0' || c > '9') {
			return false
		}
	}

	// check the first character is ascii
	if username[0] < 'a' || username[0] > 'z' {
		return false
	}

	return true
}

func (s *BlogServerBackend) isPasswordValid(password string) bool {
	return len(password) >= 8
}

// Deprecated:
func (s *BlogServerBackend) IsValidPageName(name string) bool {
	if len(name) == 0 {
		return false
	}
	for _, c := range name {
		if c < 'a' || c > 'z' && (c < '0' || c > '9') && c != '_' {
			return false
		}
	}
	return true
}

func (s *BlogServerBackend) isAdministrator(userId UserID) (bool, error) {
	// Check if user is an administrator

	res, err := s.db.Query("SELECT authlevel FROM admins WHERE userid = ?", userId)
	defer res.Close()

	if err != nil {
		return false, err
	}

	if res.Next() {
		// get the auth level

		var authLevel int
		err = res.Scan(&authLevel)
		if err != nil {
			return false, err
		}

		if authLevel >= 4 {
			return true, nil
		}
	}

	return false, nil
}

func (s *BlogServerBackend) recordLoginSuccess2(username, ip, useragent, cookie string) {
	// Record into log_logins
	_, err := s.db.Exec("INSERT INTO log_logins (userid, ip, time, success, useragent, cookie) VALUES (?, ?, ?, ?, ?, ?)", username, ip, time.Now().Unix(), 1, useragent, cookie)
	if err != nil {
		s.RecordServerError("recordLoginSuccess2", err)
	}
	log.Printf("User %s logged in from %s", username, ip)
}

func (s *BlogServerBackend) getPostMetadata(postID PostID) (post PostMetadata, err error) {
	log.Printf("Getting post metadata for post %d", postID)
	row := s.db.QueryRow("SELECT id, title, created, author FROM posts WHERE id = ? AND disabled = 0", postID)
	err = row.Scan(&post.ID, &post.Title, &post.Created, &post.AuthorID)
	if err != nil {
		s.RecordServerError("getPostMetadata", err)
		return
	}
	return
}

func (s *BlogServerBackend) GetPostsMetadata(page PageID, pagesize PageID) (results []PostMetadata, err error) {
	log.Printf("Getting posts metadata (page %d, pagesize %d)", page, pagesize)
	var offset int64
	if page == 0 {
		page = 1
	}
	offset = int64(page-1) * int64(pagesize)
	rows, err := s.db.Query("SELECT id, title, created, author FROM posts WHERE disabled = 0 ORDER BY created DESC LIMIT ? OFFSET ?", pagesize, offset)
	defer rows.Close()

	log.Printf("Query done")
	if err != nil {
		s.RecordServerError("getPostsMetadata", err)
		return
	}

	for rows.Next() {
		log.Printf("Found post")
		var post PostMetadata
		err = rows.Scan(&post.ID, &post.Title, &post.Created, &post.AuthorID)
		if err != nil {
			s.RecordServerError("getPostsMetadata", err)
			results = nil
			return
		}
		results = append(results, post)
	}

	return
}

func (s *BlogServerBackend) UserPermissionPost(author UserID) bool {
	// TODO: Fix this so not everyone can post
	return true
}
