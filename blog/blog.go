package main

// import http and sqlite3

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"html"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"reflect"
	"rpnx/go_experiments/pkg/blogbackend"
	"strconv"
	"strings"
	"sync"
	"time"
)

type WebError interface {
	error
	HttpCode() int
	HttpError() string
}

type NavbarElement interface {
	NavbarGenerateHtml(*strings.Builder, *BlogServerHttpFrontend, *requestContextInfo) error
}

type NavbarLinkElement struct {
	Name string
	Link string
}

type NavbarCurrentLoggedInUser struct {
}

type requestContextInfo struct {
	fe       *BlogServerHttpFrontend
	user     *blogbackend.UserID
	session  *blogbackend.SessionID
	request  *http.Request
	username *string
}

func (r *requestContextInfo) Check() {
	if r.fe == nil {
		panic("fe is nil")
	}
	if r.request == nil {
		panic("req is nil")
	}
}

func (r *requestContextInfo) User() blogbackend.UserID {
	if r.user == nil {
		session := r.Session()
		r.user = new(blogbackend.UserID)
		*r.user, _, _ = r.fe.Backend.GetSession(session)
	}

	return *r.user
}

func (r *requestContextInfo) Session() blogbackend.SessionID {
	if r.session == nil {
		session, err := r.fe.GetSession(r.request)
		if err != nil {
			panic(err)
		}
		r.session = &session
	}

	return *r.session
}

func (r *requestContextInfo) Username() string {
	if r.username == nil {
		var usernamestr string
		r.username = &usernamestr
		*r.username, _ = r.fe.Backend.GetUsernameFromID(r.User())
	}

	return *r.username
}

func (e *NavbarLinkElement) NavbarGenerateHtml(sb *strings.Builder, fe *BlogServerHttpFrontend, info *requestContextInfo) error {
	sb.WriteString("<a class=\"navbar-link\" href=\"")
	// TODO: Is this the right way to handle the link part?
	sb.WriteString(html.EscapeString(e.Link))
	sb.WriteString("\">")
	sb.WriteString(html.EscapeString(e.Name))
	sb.WriteString("</a>")
	return nil
}

func (e *NavbarCurrentLoggedInUser) NavbarGenerateHtml(sb *strings.Builder, fe *BlogServerHttpFrontend, info *requestContextInfo) error {
	var username string
	username = info.Username()

	if username == "" {
		username = "<Not logged in>"
	}
	//sb.WriteString("<span class=\"navbar-user\">")
	sb.WriteString(html.EscapeString("Logged in as: " + username))
	//sb.WriteString("</span>")
	return nil
}

type BlogServerHttpFrontend struct {
	Backend  *blogbackend.BlogServerBackend
	RootPath string
	handlers http.ServeMux

	navbarElements []NavbarElement
}

var mtx sync.Mutex

func (fe *BlogServerHttpFrontend) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	//print cookies
	mtx.Lock()
	defer mtx.Unlock()

	log.Printf("Incoming request URL %s, Cookies: %v", r.URL, r.Cookies())
	log.Printf("URL:" + r.URL.String())

	startTime := time.Now()
	fe.handlers.ServeHTTP(w, r)
	endTime := time.Now()
	log.Printf("%s %s %s %s", r.RemoteAddr, r.Method, r.URL, endTime.Sub(startTime))
}

func (s *BlogServerHttpFrontend) PushHtmlPage(w http.ResponseWriter, info *requestContextInfo, title string, additionalHeaderGenerator *func(*strings.Builder) error, bodyGenerator func(*strings.Builder) error) {
	html, err := s.GenerateHtmlPage(info, title, additionalHeaderGenerator, bodyGenerator)
	if err != nil {
		// check if err is a WebError
		if webErr, ok := err.(WebError); ok {
			log.Printf("WebError: %s", webErr.Error())
			http.Error(w, webErr.Error(), webErr.HttpCode())
			return
		}
		log.Printf("Failed to generate html page: %s", err.Error())
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "text/html")
	w.Write([]byte(html))

}

func (s *BlogServerHttpFrontend) GenerateHtmlPage(info *requestContextInfo, title string, additionalHeaderGenerator *func(*strings.Builder) error, bodyGenerator func(*strings.Builder) error) (string, error) {

	// user stringbuilder
	var result strings.Builder
	result.WriteString("<!DOCTYPE html>")
	result.WriteString("<html>")
	result.WriteString("<head>")
	result.WriteString("<meta charset=\"utf-8\">")
	result.WriteString(s.getStyleSheetHtml("general"))
	result.WriteString(s.getStyleSheetHtml("navbar"))
	result.WriteString(s.getStyleSheetHtml("posts"))
	result.WriteString("<script src=\"/static/timestamp-localizer.js\"></script>")
	result.WriteString("<title>")
	result.WriteString(XMLEscape(title))
	result.WriteString("</title>")
	if err := s.GenerateNavbarHtml(&result, info); err != nil {
		return "", err
	}
	// add additional header
	if additionalHeaderGenerator != nil {
		err := (*additionalHeaderGenerator)(&result)
		if err != nil {
			return "", err
		}
	}

	result.WriteString("</head>")
	result.WriteString("<body>")
	err := bodyGenerator(&result)
	if err != nil {
		return "", err
	}
	result.WriteString("</body>")
	result.WriteString("</html>")
	return result.String(), nil
}

func (s *BlogServerHttpFrontend) returnPostList2(writer http.ResponseWriter, request *http.Request, page blogbackend.PageID) {
	var pageSize blogbackend.PageID = 2
	info := s.Info(request)
	s.PushHtmlPage(writer, info, "Posts", nil, func(result *strings.Builder) error {
		// get all posts
		posts, err := s.Backend.GetPostsMetadata(page, pageSize)

		if err != nil {
			return err
		}

		// we need to build a webpage with all posts

		var bodyContent string
		for _, post := range posts {
			str, err := s.PostSummaryHtml(&post)
			if err != nil {
				return err
			}
			bodyContent += str
		}

		postCount, err := s.Backend.GetPostCount()
		if err != nil {
			return err
		}

		pageCount := blogbackend.PageID((int64(postCount) + int64(pageSize) - 1) / int64(pageSize))

		if pageCount == 0 {
			pageCount = 1
		}

		var startPage blogbackend.PageID
		var endPage blogbackend.PageID

		startPage = 1
		endPage = blogbackend.PageID(pageCount)

		if page > 5 {
			startPage = page - 5
		}

		if pageCount > 5 && page < pageCount-5 {
			endPage = page + 5
		}

		bodyContent += s.paginationHtml(startPage, page, endPage)

		result.WriteString(bodyContent)

		return nil
	})
	return
}

// Deprecated: user returnPostList2 instead

func (s *BlogServerHttpFrontend) handlePosts(writer http.ResponseWriter, request *http.Request) {
	// url should start with /postd/
	if !strings.HasPrefix(request.URL.Path, "/posts/") && request.URL.Path != "/posts" {
		http.Error(writer, "Internal server error", http.StatusInternalServerError)
		s.recordServerError("handlePages", errors.New("url does not start with /posts/"))
	}

	// get post name from url
	pageIdStr := request.URL.Path[len("/posts/"):]
	log.Printf("Post %s requested by %s", pageIdStr, request.RemoteAddr)

	// if no postname, list the posts

	if pageIdStr == "" || pageIdStr == "/" {
		s.returnPostList2(writer, request, 1)
		return
	}

	var page blogbackend.PageID
	var err error
	var pageI int64
	pageI, err = strconv.ParseInt(pageIdStr, 10, 64)
	page = blogbackend.PageID(pageI)
	if err != nil {
		http.Error(writer, "Invalid page number", http.StatusBadRequest)
		return
	}

	s.returnPostList2(writer, request, page)
	return
}

func (s *BlogServerHttpFrontend) handleStatic(writer http.ResponseWriter, request *http.Request) {
	// assert that url starts with /static/

	log.Printf("Static file requested: %s", request.URL.Path)

	if !strings.HasPrefix(request.URL.Path, "/static/") {
		// internal server error
		http.Error(writer, "Internal server error", http.StatusInternalServerError)
		s.recordServerError("handleStatic", errors.New("url does not start with /static/"))
		return
	}

	// get file name from url
	fileName := request.URL.Path[len("/static/"):]

	log.Printf("File name: %s", fileName)

	// Sanitize the file name
	fileName = path.Clean(fileName)
	log.Printf("Cleaned file name: %s", fileName)

	if strings.Index(fileName, "..") != -1 {
		http.Error(writer, "Invalid file name", http.StatusBadRequest)
		return
	}

	// sanitize the file name
	var fileNameNew string

	for _, c := range fileName {
		if c >= 'a' && c <= 'z' {
			fileNameNew += string(c)
		} else if c >= 'A' && c <= 'Z' {
			fileNameNew += string(c)
		} else if c >= '0' && c <= '9' {
			fileNameNew += string(c)
		} else if c == '.' || c == '_' || c == '-' {
			fileNameNew += string(c)
		} else {
			// invalid character
			http.Error(writer, "Invalid file name", http.StatusBadRequest)
			return
		}

	}

	// get file extension
	fileExt := filepath.Ext(fileName)
	fileExt = fileExt[1:]

	log.Printf("File extension: %s", fileExt)
	if fileExt == "css" {
		writer.Header().Set("Content-Type", "text/css")
	} else if fileExt == "js" {
		writer.Header().Set("Content-Type", "application/javascript")
	} else if fileExt == "png" {
		writer.Header().Set("Content-Type", "image/png")
	} else if fileExt == "jpg" {
		writer.Header().Set("Content-Type", "image/jpg")
	} else if fileExt == "gif" {
		writer.Header().Set("Content-Type", "image/gif")
	} else if fileExt == "svg" {
		writer.Header().Set("Content-Type", "image/svg+xml")
	} else if fileExt == "ico" {
		writer.Header().Set("Content-Type", "image/x-icon")
	} else if fileExt == "woff" {
		writer.Header().Set("Content-Type", "font/woff")
	} else if fileExt == "woff2" {
		writer.Header().Set("Content-Type", "font/woff2")
	} else if fileExt == "ttf" {
		writer.Header().Set("Content-Type", "font/ttf")
	} else if fileExt == "otf" {
		writer.Header().Set("Content-Type", "font/otf")
	} else if fileExt == "eot" {
		writer.Header().Set("Content-Type", "font/eot")
	} else {
		http.Error(writer, "404 Page Not Found", http.StatusNotFound)
		return
	}

	var file *os.File
	var err error
	file, err = os.Open(s.RootPath + "/static/" + fileName)
	if err != nil {
		http.Error(writer, "404 Page Not Found", http.StatusNotFound)
		return
	}
	defer file.Close()

	_, err = io.Copy(writer, file)
	if err != nil {
		http.Error(writer, "Internal server error", http.StatusInternalServerError)
		s.recordServerError("handleStatic", err)
		return
	}

	return
}

func (s *BlogServerHttpFrontend) handlePages(writer http.ResponseWriter, request *http.Request) {

	// assert that url starts with /pages/
	if !strings.HasPrefix(request.URL.Path, "/pages/") {
		// internal server error
		http.Error(writer, "Internal server error", http.StatusInternalServerError)
		s.recordServerError("handlePages", errors.New("url does not start with /pages/"))
		return
	}

	// get page name from url
	pageName := request.URL.Path[len("/pages/"):]
	log.Printf("Page %s requested by %s", pageName, request.RemoteAddr)

	// Check if pagename is valid
	if !s.Backend.IsValidPageName(pageName) {
		http.Error(writer, "404 Page Not Found", http.StatusNotFound)
		return
	}

	// read page from file
	pagePath := s.RootPath + "/pages/" + pageName + ".html"

	// print the body if there is one
	if request.Body != nil {
		body, err := ioutil.ReadAll(request.Body)
		if err != nil {
			log.Printf("Failed to read request body: %s", err.Error())
			http.Error(writer, "Internal error", http.StatusInternalServerError)
			return
		}
		log.Printf("Body: \n%s", body)
	}

	if request.Method == "HEAD" {
		// Tell it the length and so on
		fileInfo, err := os.Stat(pagePath)
		if err != nil {
			http.Error(writer, "Internal error", http.StatusInternalServerError)
			return
		}
		writer.Header().Set("Content-Length", strconv.FormatInt(fileInfo.Size(), 10))
		writer.Header().Set("Content-Type", "text/html")
		writer.WriteHeader(http.StatusOK)
		return
	}

	// Require is GET
	if request.Method != "GET" {
		http.Error(writer, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	pageData, err := ioutil.ReadFile(pagePath)
	if err != nil {
		http.Error(writer, "Page not found", http.StatusNotFound)
		return
	}

	// write page to response
	writer.Header().Set("Content-Type", "text/html")
	writer.Write(pageData)

}

func (s *BlogServerHttpFrontend) ApiRegister(msg *UserRegisterRequest, writer http.ResponseWriter, request *http.Request) {

	// Register user
	err := s.Backend.Register(msg.Username, msg.Password, msg.Email, msg.Phone)
	if err != nil {
		log.Printf("Failed to register user: %s", err)
		s.PushHttpError(writer, err)
		return
	}

	http.Redirect(writer, request, "/login", http.StatusSeeOther)
}

func (s2 *BlogServerHttpFrontend) recordServerError(s string, err error) {
	s2.Backend.RecordServerError(s, err)
}

func GetIPFromRequest(request *http.Request) string {
	var remoteAddr string = request.RemoteAddr
	var ipAddress string
	// find the last occurence of :
	index := strings.LastIndex(remoteAddr, ":")
	if index != -1 {
		ipAddress = remoteAddr[:index]
	} else {
		panic("unknown remote address format")
	}
	return ipAddress
}

// Deprecated: User GenerateNavbarHtml instead
func (s *BlogServerHttpFrontend) NavbarHtml(info *requestContextInfo) (string, error) {
	info.Check()
	var builder strings.Builder
	err := s.GenerateNavbarHtml(&builder, info)
	return builder.String(), err
}

func (s *BlogServerHttpFrontend) GenerateNavbarHtml(builder *strings.Builder, info *requestContextInfo) error {
	info.Check()
	log.Printf("Generating navbar html")
	builder.WriteString("<div class=\"navbar\">")
	for _, page := range s.navbarElements {
		builder.WriteString("<span class=\"navbar-element\">")
		page.NavbarGenerateHtml(builder, s, info)
		builder.WriteString("</span>")
	}
	builder.WriteString("</div>")
	return nil
}

func (s *BlogServerHttpFrontend) PostFullHtml(post *blogbackend.Post) (string, error) {
	result := ""

	result += "<div class=\"post-full\">"
	result += "<h2>" + html.EscapeString(post.Metadata.Title) + "</h2>"

	var authorName string
	authorName, err := s.Backend.GetUsernameFromID(post.Metadata.AuthorID)

	if err != nil {
		return "", err
	}

	result += "<p>Author: " + html.EscapeString(authorName) + "</p>"
	result += "<p>Date: " + html.EscapeString(post.Metadata.Created.String()) + "</p>"
	result += "<p class=\"post-full-content\">" + html.EscapeString(post.Content) + "</p>"
	result += "</div>"

	return result, nil
}

func (s *BlogServerHttpFrontend) paginationHtml(startPage blogbackend.PageID, currentPage blogbackend.PageID, maxPage blogbackend.PageID) (result string) {
	// A result like, eg 1 | 2 | 3 | 4
	// where all pages except the current page are links

	result += "<div class=\"pagination\">Page: [ "
	for i := startPage; i <= maxPage; i++ {
		if i == currentPage {
			result += fmt.Sprintf("<span class=\"pagination-current\">%d</span>", i)
		} else {
			result += fmt.Sprintf("<span class=\"pagination-other\"><a href=\"/posts/%d\">%d</a></span>", i, i)
		}

		if i != maxPage {
			result += " | "
		}
	}

	result += " ]</div>"
	return result

}

func (s *BlogServerHttpFrontend) PostFullPageHtml(post *blogbackend.Post, info *requestContextInfo) (result string, err error) {
	result += "<!DOCTYPE html>"
	result += "<html>"
	result += "<head>"
	result += s.getStyleSheetHtml("general")
	result += s.getStyleSheetHtml("posts")
	result += s.getStyleSheetHtml("navbar")
	result += "<title>" + html.EscapeString(post.Metadata.Title) + "</title>"
	result += "</head>"
	result += "<body>"
	navbar, err := s.NavbarHtml(info)
	if err != nil {
		return "", err
	}
	result += navbar

	result += "<div class=\"page-content\">"
	var postHtml string
	postHtml, err = s.PostFullHtml(post)
	if err != nil {
		return "", err
	}
	result += postHtml
	result += "</div>"

	result += "</div>"
	result += "</body>"
	result += "</html>"

	return result, nil

}

// Deprecated: Use GeneratePostSummaryHtml instead
func (s *BlogServerHttpFrontend) PostSummaryHtml(post *blogbackend.PostMetadata) (result string, err error) {
	result += "<div class=\"post-summary\">"
	titleHtml := html.EscapeString(post.Title)
	result += fmt.Sprintf("<h2><a href=\"/post/%d\">%s</a></h2>", post.ID, titleHtml)
	var authorName string
	authorName, err = s.Backend.GetUsernameFromID(post.AuthorID)

	if err != nil {
		return "", err
	}

	var authorNameHtml = html.EscapeString(authorName)

	result += fmt.Sprintf("<p class=\"post-summary-author\">Author: %s</p>", authorNameHtml)

	var dateHtml = html.EscapeString(post.Created.String())
	result += fmt.Sprintf("<p class=\"post-summary-date\">Date: %s</p>", dateHtml)

	result += "</div>"

	return result, nil

}

func (s *BlogServerHttpFrontend) GeneratePostSummaryHtml(builder *strings.Builder, post *blogbackend.PostMetadata) error {
	builder.WriteString("<div class=\"post-summary\">")
	titleHtml := html.EscapeString(post.Title)
	builder.WriteString(fmt.Sprintf("<h2><a href=\"/post/%d\">%s</a></h2>", post.ID, titleHtml))
	authorName, err := s.Backend.GetUsernameFromID(post.AuthorID)

	if err != nil {
		return err
	}

	authorNameHtml := html.EscapeString(authorName)

	builder.WriteString(fmt.Sprintf("<p class=\"post-summary-author\">Author: %s</p>", authorNameHtml))

	dateHtml := html.EscapeString(post.Created.String())
	builder.WriteString(fmt.Sprintf("<p class=\"post-summary-date\">Date: %s</p>", dateHtml))

	builder.WriteString("</div>")

	return nil
}

func (s *BlogServerHttpFrontend) handlePost(writer http.ResponseWriter, request *http.Request) {
	// make sure this is a get

	if request.Method != "GET" {
		http.Error(writer, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	info := &requestContextInfo{}
	info.request = request
	info.fe = s
	info.Check()

	// get the post id
	postIdStr := request.URL.Path[len("/post/"):]

	// make sure it's a valid post id (number only)
	if !s.Backend.IsValidPostId(postIdStr) {
		http.Error(writer, "Invalid post ID", http.StatusBadRequest)
		return
	}
	// convert to number

	id, err := strconv.Atoi(postIdStr)
	if err != nil {
		http.Error(writer, "Invalid post ID", http.StatusBadRequest)
		return
	}

	var postId blogbackend.PostID = blogbackend.PostID(id)
	var postHtml string
	var post *blogbackend.Post
	post, err = s.Backend.GetPostFullData(postId)
	if err != nil {
		http.Error(writer, "Error getting post", http.StatusInternalServerError)
		return
	}
	// TODO: User information
	postHtml, err = s.PostFullPageHtml(post, info)

	if err != nil {
		http.Error(writer, "Error getting post", http.StatusInternalServerError)
		return
	}

	writer.Write([]byte(postHtml))
	return

}

func (s *BlogServerHttpFrontend) getStyleSheetHtml(s2 string) string {
	return "<link rel=\"stylesheet\" type=\"text/css\" href=\"/static/stylesheet-" + s2 + ".css\">"
}

func (s *BlogServerHttpFrontend) FormPageHandler(title string, typ reflect.Type) (result func(http.ResponseWriter, *http.Request)) {
	var lowerTitle string
	lowerTitle = strings.ToLower(title)
	formHtml, err := GenerateForm(typ, title, "/api/"+lowerTitle, "POST")
	if err != nil {
		panic("Error generating form: " + err.Error())
	}
	srv := s
	return func(writer http.ResponseWriter, request *http.Request) {
		info := srv.Info(request)
		if request.Method != "GET" {
			http.Error(writer, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}
		var pageHtml string
		pageHtml, err = srv.GenerateHtmlPage(info, title, nil, func(builder *strings.Builder) error {
			builder.WriteString(formHtml)
			return nil
		})
		if err != nil {
			http.Error(writer, "Error generating page", http.StatusInternalServerError)
			return
		}
		writer.Write([]byte(pageHtml))
	}
}

func (s *BlogServerHttpFrontend) ApiPublishPost(msg *UserNewPostRequest, writer http.ResponseWriter, r *http.Request) {
	info := s.Info(r)
	// check is POST
	if r.Method != "POST" {
		http.Error(writer, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	id, err := s.Backend.SessionPublishPost(info.Session(), msg.Title, msg.Content)
	if err != nil {
		s.PushHttpError(writer, err)
		return
	}
	var url string
	url = fmt.Sprintf("/post/%d", id)
	http.Redirect(writer, r, url, http.StatusSeeOther)

}

func (s *BlogServerHttpFrontend) handleLogin(writer http.ResponseWriter, request *http.Request) {

	var pageHtml string
	info := s.Info(request)

	pageHtml, err := s.GenerateHtmlPage(info, "Login", nil, func(builder *strings.Builder) error {
		formhtml, err := GenerateForm(reflect.TypeOf(UserLoginRequest{}), "Login", "/api/login", "POST")
		if err != nil {
			return err
		}
		builder.WriteString(formhtml)
		return nil
	})
	if err != nil {
		http.Error(writer, "Error generating page", http.StatusInternalServerError)
		return
	}

	writer.Write([]byte(pageHtml))
	return
}

func (s *BlogServerHttpFrontend) PushHttpError(writer http.ResponseWriter, err error) {
	if weberr, ok := err.(WebError); ok {
		http.Error(writer, weberr.HttpError(), weberr.HttpCode())
	} else {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
	}
}

func (s *BlogServerHttpFrontend) Close() {
	// Doesn't do anything at the moment
}

func (s *BlogServerHttpFrontend) ApiLogin(msg *UserLoginRequest, writer http.ResponseWriter, request *http.Request) {
	log.Printf("Login request: %v", msg)

	options := blogbackend.DefaultLoginOptions()
	options.Remember = msg.RememberMe

	var session blogbackend.SessionID
	var err error
	session, err = s.Backend.Login(msg.Username, msg.Password, options)

	log.Printf("Session: %v", session)

	if err != nil {
		s.PushHttpError(writer, err)
		return
	}

	log.Printf("Login OK!")

	cookie := http.Cookie{
		Name:     "session_cookie",
		Value:    string(session),
		MaxAge:   36000,
		Path:     "/",
		Secure:   true,
		HttpOnly: true,
	}
	http.SetCookie(writer, &cookie)
	http.Redirect(writer, request, "/posts", http.StatusSeeOther)
}

func (s *BlogServerHttpFrontend) FormEndpoint(title string, f any) {
	var lowerTitle string
	lowerTitle = strings.ToLower(title)
	// get the first argument type of f
	f_type := reflect.TypeOf(f)
	if f_type.Kind() != reflect.Func {
		panic("f is not a function")
	}
	if f_type.NumIn() != 3 {

	}

	arg0_type := f_type.In(0)
	if arg0_type.Kind() != reflect.Ptr {
		panic("Invalid interface")
	}
	arg0_type = arg0_type.Elem()
	arg1_type := f_type.In(1)
	// make sure arg2 == http.ResponseWriter
	if arg1_type != reflect.TypeOf((*http.ResponseWriter)(nil)).Elem() {
		panic("Invalid interface")
	}
	// make sure arg3 == *http.Request
	arg2_type := f_type.In(2)
	if arg2_type != reflect.TypeOf((*http.Request)(nil)) {
		log.Printf("arg2_type: %v", arg2_type)
		panic("Invalid interface")
	}

	s.handlers.Handle("/"+lowerTitle, http.HandlerFunc(s.FormPageHandler(title, arg0_type)))
	s.handlers.Handle("/api/"+lowerTitle, http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		if request.Method != "POST" {
			http.Error(writer, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}
		val, err := HttpRequestParseForm(request, arg0_type)
		if err != nil {
			s.PushHttpError(writer, err)
			return
		}
		// call the function
		f_val := reflect.ValueOf(f)
		f_val.Call([]reflect.Value{reflect.ValueOf(val), reflect.ValueOf(writer), reflect.ValueOf(request)})
	}))
}

func (s *BlogServerHttpFrontend) ApiMainpage(writer http.ResponseWriter, request *http.Request) {
	http.Redirect(writer, request, "/posts", http.StatusSeeOther)
}

// TODO: Convert to New
func (s *BlogServerHttpFrontend) Init(backend *blogbackend.BlogServerBackend, path string) error {
	s.Backend = backend
	s.RootPath = path
	s.handlers.Handle("/", http.HandlerFunc(s.ApiMainpage))
	s.handlers.Handle("/pages/", http.HandlerFunc(s.handlePages))
	s.handlers.Handle("/posts/", http.HandlerFunc(s.handlePosts))
	s.handlers.Handle("/post/", http.HandlerFunc(s.handlePost))
	s.handlers.Handle("/static/", http.HandlerFunc(s.handleStatic))

	s.FormEndpoint("Login", s.ApiLogin)
	s.FormEndpoint("NewPost", s.ApiPublishPost)
	s.FormEndpoint("Register", s.ApiRegister)
	s.AddNavbarElement("Home", "/posts")
	s.AddNavbarElement("Register", "/register")
	s.AddNavbarElement("New Post", "/newpost")
	s.AddNavbarElement("Login", "/login")
	s.navbarElements = append(s.navbarElements, &NavbarCurrentLoggedInUser{})
	return nil
}

func (fe *BlogServerHttpFrontend) AddNavbarElement(name string, url string) {
	fe.navbarElements = append(fe.navbarElements, &NavbarLinkElement{name, url})
}

func (fe *BlogServerHttpFrontend) GetSession(r *http.Request) (blogbackend.SessionID, error) {
	log.Printf(("GetSession"))
	cookie, err := r.Cookie("session_cookie")
	log.Printf("Cookie: %v", cookie)

	if err == http.ErrNoCookie {
		return blogbackend.SessionID(""), nil
	}
	if err != nil {
		return blogbackend.SessionID(""), err
	}

	return blogbackend.SessionID(cookie.Value), nil
}

func (fe *BlogServerHttpFrontend) Info(request *http.Request) *requestContextInfo {
	info := &requestContextInfo{}
	info.request = request
	info.fe = fe
	return info
}

func PanicIfErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {

	var database *sql.DB
	var err error

	home, err := os.UserHomeDir()
	PanicIfErr(err)
	wwwDir := filepath.Join(home, "www")

	databasePath := filepath.Join(wwwDir, "blog.db")

	err = os.MkdirAll(wwwDir, 0755)
	PanicIfErr(err)

	database, err = sql.Open("sqlite3", databasePath)
	if err != nil {
		log.Fatal(err)
	}

	backend, err := blogbackend.New(database)
	defer backend.Close()
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Created server backend")
	if err != nil {
		log.Fatal(err)
	}

	var frontend *BlogServerHttpFrontend
	frontend = &BlogServerHttpFrontend{}

	err = frontend.Init(backend, "/Users/rnicholl/www")
	defer frontend.Close()
	if err != nil {
		log.Fatal(err)
	}

	ms := http.NewServeMux()
	ms.Handle("localhost/", frontend)
	ms.Handle("127.0.0.1/", frontend)

	//http.ListenAndServe(":8080", ms)

	err = http.ListenAndServeTLS(":443", "/Users/rnicholl/www/localhost.crt", "/Users/rnicholl/www/localhost.key", ms)
	log.Fatal(err)
}
