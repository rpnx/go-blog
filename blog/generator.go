package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"html"
	"net/http"
	"reflect"
	"strings"
)

func XMLEscape(s string) string {
	var buf bytes.Buffer
	err := xml.EscapeText(&buf, []byte(s))
	if err != nil {
		panic(err)
	}
	return buf.String()
}

type UserLoginRequest struct {
	Username   string `form_inputtype:"text" form_displayname:"Username" form_required:"true"`
	Password   string `form_inputtype:"password" form_displayname:"Password" form_required:"true"`
	RememberMe bool   `form_inputtype:"checkbox" form_displayname:"Remember Me" form_default:"false"`
}

type UserRegisterRequest struct {
	Username string `form_inputtype:"text" form_displayname:"Username" form_required:"true"`
	Password string `form_inputtype:"password" form_displayname:"Password" form_required:"true"`
	Email    string `form_inputtype:"email" form_displayname:"Email" form_required:"true"`
	Phone    string `form_inputtype:"phone" form_displayname:"Phone" form_required:"false"`
}

type UserNewPostRequest struct {
	Title   string `form_inputtype:"text" form_displayname:"Title" form_required:"true"`
	Content string `form_inputtype:"text" form_displayname:"Content" form_required:"true"`
}

func HttpRequestParseForm(r *http.Request, t reflect.Type) (interface{}, error) {
	var err error
	var result interface{}
	result = reflect.New(t).Interface()
	err = r.ParseForm()
	if err != nil {
		return nil, err
	}
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		name := strings.ToLower(field.Name)
		tag := field.Tag.Get("form_inputtype")
		if tag == "checkbox" {
			// handle checkbox
			val, ok := r.Form[name]
			if !ok || len(val) == 0 {
				defaultValue := field.Tag.Get("form_default")
				if defaultValue == "" {
					defaultValue = "false"
				}
				if defaultValue == "true" {
					reflect.ValueOf(result).Elem().FieldByName(field.Name).SetBool(true)
				} else {
					reflect.ValueOf(result).Elem().FieldByName(field.Name).SetBool(false)
				}
				// get default value
				continue
			} else {
				if val[0] == "on" || val[0] == "true" {
					reflect.ValueOf(result).Elem().FieldByName(field.Name).SetBool(true)
				} else {
					reflect.ValueOf(result).Elem().FieldByName(field.Name).SetBool(false)
				}
			}

		} else if tag == "radio" {
			// handle radio
			optionNamesList := getFieldOptionsOrPanic(field)
			// get the value of the radio button
			value := r.FormValue(name)
			// check if the value is valid
			if !contains(optionNamesList, value) {
				return nil, fmt.Errorf("Invalid value for radio button %s", name)
			}
			// set the value
			reflect.ValueOf(result).Elem().FieldByName(field.Name).SetString(value)
		} else {
			// handle text, password, email, phone
			value := r.FormValue(name)
			reflect.ValueOf(result).Elem().FieldByName(field.Name).SetString(value)
		}
	}
	return result, nil
}

func contains(list []string, value string) bool {
	for _, item := range list {
		if item == value {
			return true
		}
	}
	return false
}

func GenerateForm(t reflect.Type, submitLabel string, url string, method string) (string, error) {

	var htmlResult string
	var first bool = true
	htmlResult = "<form action=\"" + url + "\" method=\"" + method + "\">"
	for i := 0; i < t.NumField(); i++ {
		if !first {
			//htmlResult += "<br>"
		} else {
			first = false
		}
		field := t.Field(i)
		name := strings.ToLower(field.Name)

		// get type htmlinputtype tag
		tag := field.Tag.Get("form_inputtype")

		displayName := field.Tag.Get("form_displayname")
		if displayName == "" {
			displayName = field.Name
		}

		requiredStr := field.Tag.Get("form_required")
		if requiredStr == "true" {
		} else if requiredStr == "false" || requiredStr == "" {
			requiredStr = "false"
		}

		// create a stringWriter to write xml to

		htmlResult += fmt.Sprintf("<label for=%s>%s</label>", XMLEscape(name), html.EscapeString(displayName))

		if tag == "checkbox" {
			// handle checkbox

			defaultStr := field.Tag.Get("form_default")
			if defaultStr == "" {
				defaultStr = "false"
			}
			if requiredStr == "false" {
				requiredStr = ""
			} else {
				requiredStr = "required"
			}
			htmlResult += fmt.Sprintf(`<input type="checkbox" name="%s" %s value="%s">`, name, requiredStr, defaultStr)

		} else if tag == "radio" {
			// handle radio
			optionNamesList := getFieldOptionsOrPanic(field)

			// create a radio button for each option
			for _, optionName := range optionNamesList {
				htmlResult += fmt.Sprintf(`<input type="radio" name="%s" value="%s">`, name, optionName)
			}

		} else if tag == "select" {

			optionNamesList := getFieldOptionsOrPanic(field)

			for _, optionName := range optionNamesList {
				htmlResult += fmt.Sprintf(`<option value="%s">%s</option>`, optionName, optionName)
			}

		} else if tag == "textarea" {
			// handle textarea
			htmlResult += fmt.Sprintf(`<textarea name="%s" required="%s"></textarea>`, name, requiredStr)
		} else if tag == "phone" {
			// handle phone
			htmlResult += fmt.Sprintf(`<input type="tel" name="%s" required="%s">`, name, requiredStr)
		} else if tag == "email" {
			htmlResult += fmt.Sprintf(`<input type="email" name="%s" required="%s">`, name, requiredStr)
		} else if tag == "password" {
			htmlResult += fmt.Sprintf(`<input type="password" name="%s" required="%s">`, name, requiredStr)
		} else if tag == "text" {
			// handle text
			htmlResult += fmt.Sprintf(`<input type="text" name="%s" required="%s">`, name, requiredStr)
		} else {
			return "", fmt.Errorf("unknown input type %s", tag)
		}
	}

	htmlResult += fmt.Sprintf(`<input type="submit" value="%s">`, submitLabel)
	htmlResult += "</form>"

	return htmlResult, nil

}

func getFieldOptions(field reflect.StructField) ([]string, error) {
	var optionsJson string = field.Tag.Get("htmloptions")
	// options is a json array of option strings
	// e.g. `htmloptions:"[\"option1\", \"option2\"]"`

	var optionNamesList []string

	// JSON parse the string optionsJson into a list of strings

	err := json.Unmarshal([]byte(optionsJson), &optionNamesList)
	if err != nil {
		return nil, err
	}
	return optionNamesList, nil
}

func getFieldOptionsOrPanic(field reflect.StructField) []string {
	optionNamesList, err := getFieldOptions(field)
	if err != nil {
		panic(err)
	}
	return optionNamesList
}
